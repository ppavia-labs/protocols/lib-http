package ppa.httpstream.model;

public interface HttpStreamBuilder {

	/**
	 * extract meta from the first line of http stream (method, protocol version, ...)
	 * @param line
	 * @return
	 */
	public HttpStreamBuilder buildStartLineStream(String line);
	public HttpStreamBuilder buildHeaderParams(String line);
	public HttpStreamBuilder buildContent(String lines);
	public HttpStream build();
	public int getContentLength();
	public void setContentLength(int contentLength);
}
