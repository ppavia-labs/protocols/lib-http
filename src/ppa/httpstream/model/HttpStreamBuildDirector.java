package ppa.httpstream.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;

import ppa.http.utils.Logger;

public class HttpStreamBuildDirector {
	private HttpStreamBuilder builder;
	private static Logger<HttpStreamBuildDirector> LOG			= Logger.getLogger(HttpStreamBuildDirector.class);
	
	public HttpStreamBuildDirector (final HttpStreamBuilder builder) {
		this.builder	= builder;
	}
	
	public HttpStream constructSSL(BufferedReader br) {
		String inLine = null;
		try {
			inLine = br.readLine();
			builder.buildStartLineStream(inLine);
			LOG.debug("[first line] " + "inLine" + " : " + inLine);
			
			while ( inLine != null ) {
				inLine	= br.readLine();
				if ( inLine != null && !inLine.trim().isEmpty() ) {
					builder.buildHeaderParams(inLine);
					LOG.debug("[header] " + "inLine" + " : " + inLine);
				}
				else if ( inLine != null ) {
					inLine = String.format("%040x", new BigInteger(1, inLine.getBytes("UTF-8")));
					LOG.debug("[empty] " + "inLine" + " : " + inLine);
					inLine	= br.readLine();
					LOG.debug("[content] " + "inLine" + " : " + inLine);
					builder.buildContent(inLine);
				}
			}
		}
		catch (IOException e) {
			LOG.error("ERROR on read line\n" + e.getMessage());
		}
		return builder.build();
		
	}
	
	public HttpStream construct(BufferedReader in) {
		String inLine;
		try {
			inLine = in.readLine();
			builder.buildStartLineStream(inLine);
			LOG.info("in : " + inLine);
			while ( inLine != null ) {
				inLine	= in.readLine();
				if ( inLine != null && !inLine.isEmpty() ) {
					builder.buildHeaderParams(inLine);
					LOG.info("in : " + inLine);
				}
				else if ( inLine != null ) {
					inLine = String.format("%040x", new BigInteger(1, inLine.getBytes("UTF-8")));
					LOG.info("empty inLine : " + inLine);
					inLine	= in.readLine();
					LOG.info("content : " + inLine);
					builder.buildContent(inLine);
				}
			}
		}
		catch (IOException e) {
			LOG.error("ERROR on read line\n" + e.getMessage());
		}
		return builder.build();
		
	}
	
	public HttpStream construct(InputStream bis) {
		String inLine			= "";
		int stream;
		int lastStream				= 0;
		boolean isContent		= false;
		boolean isLineCompleted	= false;
		boolean isFirstLine		= true;
		String content			= "";
		int sizeOfContent		= 0;
		boolean isEndStream		= false;
		try {
			// read the response char by char (isEndStream = Content-Length value)
			while ( !isEndStream ) {
				stream = bis.read();
				if ( !isContent && stream == 13 && lastStream != 10 ) { // CR
					lastStream	= stream;
				}
				else if ( !isContent && lastStream == 13 && stream == 10 ) { // CRLF -> EOL
					lastStream = stream;
					isLineCompleted = true;
				}
				else if ( !isContent && (lastStream == 10 && (stream == 13 || stream == 10)) ) { // line break
					isLineCompleted = false;
					if ( stream == 10 ) { // end of line break
						isContent = true;						
					}
				}
				else {
					inLine += (char)stream;
					lastStream = stream;
				}
				if ( isLineCompleted || isContent ) {
					if ( isContent == false ) { // header
						//System.out.println("header line : " + inLine);
						if ( isFirstLine ) {
							//System.out.println("FIRST line : " + inLine);
							builder.buildStartLineStream(inLine);
							isFirstLine		= false;
						}
						else {
							builder.buildHeaderParams(inLine);
							LOG.info("header : " + inLine);
						}
						isLineCompleted = false;
						inLine = "";
					}
					else { // content
						sizeOfContent = builder.getContentLength(); // sizeOfContent == 0 : Content-Length is not present in the header of the request
						content += (char)stream;
						byte[] postData			= content.getBytes( StandardCharsets.UTF_8 );
						int    postDataLength	= postData.length;
						if ( postDataLength >= sizeOfContent || sizeOfContent == 0 ) { // stop reader
							isEndStream = true;
						}
					}
				}
			}
			LOG.info("content line : " + content);
			builder.buildContent(content);
		}
		catch (IOException e) {
			LOG.error("ERROR on read line\n" + e.getMessage());
		}
		return builder.build();
	}
	
	public HttpStream constructIO(BufferedReader in) {
		String inLine;
		try {
			LOG.info("in : " + System.currentTimeMillis());
			inLine = in.readLine();
			while ( inLine != null ) {
				inLine	= in.readLine();				
			}
			LOG.info("out : " + System.currentTimeMillis());
		}
		catch (IOException e) {
			LOG.error("ERROR on read line\n" + e.getMessage());
		}
		return builder.build();
		
	}
}
